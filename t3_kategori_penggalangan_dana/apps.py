from django.apps import AppConfig


class T3KategoriPenggalanganDanaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't3_kategori_penggalangan_dana'
