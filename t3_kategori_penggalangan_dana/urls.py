from django.urls import path
from .views import create, index, update

urlpatterns = [
    path('', index, name='index'),
    path('create', create, name='create'),
    path('update', update, name='update'),
]