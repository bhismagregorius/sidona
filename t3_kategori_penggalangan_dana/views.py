from django.shortcuts import render
from django.db import connection
from collections import namedtuple

# Function to return every row of data from query
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Create your views here.
def index(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO sidona")
        cursor.execute("SELECT * FROM KATEGORI_PD")
        result = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'indexkpd.html', {'result': result})

def create(request):
    return render(request, 'createkpd.html')

def update(request):
    id = request.GET.get('id', '')
    result = []
    if id != '':
        cursor = connection.cursor()
        try:
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute("SELECT * FROM KATEGORI_PD WHERE id = " + "'" + id + "'")
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    return render(request, 'updatekpd.html', {'result': result})