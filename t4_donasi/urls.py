from django.urls import path

from . import views

urlpatterns = [
    path('', views.donasi_view, name='index'),
    path('detail/<str:id_donasi>', views.donasi_detail_view, name='detail_donasi'),
    path('form/<str:id_donasi>', views.donasi_add_view, name='form_donasi'),
]
app_name = 't4_donasi'