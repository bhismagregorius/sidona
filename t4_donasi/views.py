import json
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from datetime import datetime


def index(request):
    return render(request, 'list_donasi.html', {})

def detail_donasi(request):
    return render(request, 'detail_donasi.html', {})

def form_donasi(request):
    return render(request, 'form_donasi.html', {})


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def donasi_view(request):
   if request.session.has_key('email'):
        cursor = connection.cursor()
        result = []
        try:
            email = request.session['email'][0]
            print(email)
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute(
                "select pd.id as idpd, pd.judul, k.nama_kategori, s.status from donasi d join penggalangan_dana_pd pd on pd.id = d.idpd join kategori_pd k on k.id = pd.id_kategori join status_pembayaran s on s.id = d.idstatuspembayaran where email='" + email + "';"
            )
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        return render(request, 'list_donasi.html', {"result" : result})
   else:
        return HttpResponseRedirect('/login/masuk')


def donasi_detail_view(request, id_donasi):
    idPD = id_donasi
    if request.session.has_key('email'):
        cursor = connection.cursor()
        result = []
        try:
            email = request.session['email'][0]
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute(
                "select * from donasi d join penggalangan_dana_pd pd on d.idpd = pd.id where d.idpd = '" + idPD + "' and d.email = '" + email + "';"
            )
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        return render(request, 'detail_donasi.html', {"result" : result})
    else:
        return HttpResponseRedirect('/login/masuk')


def donasi_add_view(request, id_donasi):
    print(id_donasi)
    if request.session.has_key('email'):
        result = []
        if request.method == 'POST':
            print("MASUK GAISKU")
            email = request.session['email'][0]
            print(email)
            idPD = id_donasi
            print(id_donasi)
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print(timestamp)
            nominal = request.POST.get('nominal')
            print(nominal)
            metode = request.POST.get('metode_bayar')
            print(metode)
            status = request.POST.get('status_anonim')
            print(status)
            doa = request.POST.get('doa')
            print(doa)
            cursor = connection.cursor()
            result = []
            try:
                cursor.execute("SET SEARCH_PATH TO sidona")
                cursor.execute(
                    "insert into donasi values ('" + email + "', '" + timestamp + "', '" + nominal + "', '" + metode +  "', '" + status + "', '" + doa + "', '" + idPD + "', '924491011');"
                )
                result = namedtuplefetchall(cursor)
                print(result)
            except Exception as e:
                print(e)
            finally:
                cursor.close()
        return render(request, 'form_donasi.html', {"result" : result})
    else:
        print('GAMASUK GAIS')
        return HttpResponseRedirect('/login/masuk')
