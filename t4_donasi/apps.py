from django.apps import AppConfig


class T4DonasiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't4_donasi'
