from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from .forms import *
from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from datetime import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Create your views here.
def create(request, id):
    form = FormPencairan(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        try:
            idpd = id
            nominal = form.cleaned_data['nominal']
            keterangan = form.cleaned_data['keterangan']
            timestamp = datetime.now()
            print(nominal, keterangan, timestamp)
            cursor = connection.cursor()
            cursor.execute("SET search_path TO sidona")
            cursor.execute("INSERT INTO RIWAYAT_PENGGUNAAN_DANA VALUES (%s, %s, %s, %s)", [idpd, timestamp, nominal, keterangan])
            cursor.close()
            return HttpResponseRedirect('')
        except Exception as e:
            print(e)
            cursor.close()
    else:
        cursor = connection.cursor()
        try:
            cursor.execute("SET SEARCH_PATH TO sidona")
            # get current id_pd somehow
            id_pd = id
            cursor.execute("SELECT id, judul, email_user FROM PENGGALANGAN_DANA_PD PD WHERE PD.ID = %s", [id_pd])
            pd = namedtuplefetchall(cursor)
            cursor.execute("SELECT email, nama, Saldo_Dona_Pay FROM penggalang_dana WHERE email = %s", [pd[0].email_user])
            user = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        # print(type(pd[0]))
        # print(pd[0].judul)

        cursor.close()
        # print(user)
        context = {'pd': pd[0], 'user': user[0], 'form':form}
        return render(request, 'create_riwayat.html', context)

def detail(request, id):
    try:
        cursor = connection.cursor()
        cursor.execute("SET SEARCH_PATH TO sidona")
        id_pd = id
        cursor.execute("SELECT * FROM penggalangan_dana_pd WHERE id = %s", [id_pd])
        pd = namedtuplefetchall(cursor)[0]
        cursor.execute("SELECT Nama_Kategori FROM kategori_pd WHERE id = %s", [pd.id_kategori])
        nama_kategori = namedtuplefetchall(cursor)[0].nama_kategori
        print(nama_kategori)
        context = {'pd': pd,'nama_kategori':nama_kategori}

        if (nama_kategori == "Kesehatan"):
            cursor.execute("SELECT * FROM pd_kesehatan WHERE idpd = %s", [id_pd])
            pd_kesehatan = namedtuplefetchall(cursor)[0] # isinya object pd_kesehatan
            cursor.execute("SELECT * FROM pasien WHERE nik = %s", [pd_kesehatan.idpasien])
            pasien = namedtuplefetchall(cursor)[0] # isinya object pasien
            cursor.execute("SELECT * FROM komorbid WHERE idpd = %s", [id_pd])
            pkomorbid = namedtuplefetchall(cursor) # isinya list of object penyakit
            print(pkomorbid[0])
            for i in range(len(pkomorbid)):
                pkomorbid[i] = pkomorbid[i].komorbid # sekarang komorbid isinya list of string 

            pkomorbid = ", ".join(pkomorbid)
            print(pkomorbid)
            context |= {'pd_kesehatan': pd_kesehatan,'pasien': pasien, 'komorbid': pkomorbid}

        elif (nama_kategori == "Rumah Ibadah"):
            cursor.execute("SELECT * FROM PD_RUMAH_IBADAH WHERE idpd = %s", [id_pd])
            pd_ibadah = namedtuplefetchall(cursor)[0]
            cursor.execute("SELECT nama FROM KATEGORI_AKTIVITAS_PD_RUMAH_IBADAH WHERE id = %s", [pd_ibadah.idaktivitas])
            kategori_aktivitas = namedtuplefetchall(cursor)[0]
            context |= {"pd_ibadah":pd_ibadah, 'kategori_aktivitas': kategori_aktivitas}

            print(context)

        cursor.close()

    except Exception as e:
        print(e)
    return render(request, 'detail_pd.html', context)
