from django.urls import path
from .views import *

urlpatterns = [
    path('<str:id>', create, name='create'),
    path('detail/<str:id>', detail, name='detail'),
]
