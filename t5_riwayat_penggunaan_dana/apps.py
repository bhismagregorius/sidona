from django.apps import AppConfig


class T5RiwayatPenggunaanDanaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't5_riwayat_penggunaan_dana'
