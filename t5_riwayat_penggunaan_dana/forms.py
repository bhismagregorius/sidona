from django import forms
from django.contrib.auth.decorators import login_required

class FormPencairan(forms.Form):
    nominal = forms.IntegerField(label='nominal')
    keterangan = forms.CharField(label='keterangan', max_length=50)