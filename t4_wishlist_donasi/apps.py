from django.apps import AppConfig


class T4WishlistDonasiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't4_wishlist_donasi'
