import json
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple


def index(request):
    return render(request, 'wishlist_donasi.html', {})


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def wishlist_view(request):
   if request.session.has_key('email'):
        cursor = connection.cursor()
        myresult = {}
        hasil = []
        data = []
        try:
            email = request.session['email'][0]
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute(
                "select pd.id, pd.judul, k.nama_kategori from penggalangan_dana_pd pd join kategori_pd k on k.id = pd.id_kategori where pd.id in (select wd.idPD from wishlist_donasi wd where wd.email = '" + email + "');"
            )
            hasil = namedtuplefetchall(cursor)
            cursor.execute(
                "select * from individu where email = '" + email + "';"
            )
            data = namedtuplefetchall(cursor)
            myresult = {
                "result" : [
                    {
                        "name" : "hasil",
                        "list" : hasil,
                    },
                    {
                        "name" : "data",
                        "list" : data,
                    }
                ]
            }
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        return render(request, 'wishlist_donasi.html', myresult)
   else:
        return HttpResponseRedirect('/login/masuk')


def wishlist_detail_view(request, id_donasi):
    idPD = id_donasi
    if request.session.has_key('email'):
        cursor = connection.cursor()
        result = []
        try:
            email = request.session['email'][0]
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute(
                "select pd.id idPD, judul, kota, provinsi, tanggal_aktif_awal, tanggal_aktif_akhir, sisa_hari, jumlah_dibutuhkan, jumlah_terkumpul, jumlah_terpakai, nama_kategori from penggalangan_dana_pd pd join kategori_pd k on pd.id_kategori = k.id where pd.id = '" + idPD + "';"
            )
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        return render(request, 'wishlist_donasi_detail.html', {"result" : result})
    else:
        return HttpResponseRedirect('/login/masuk')


def wishlist_delete_view(request, id_donasi):
    idPD = id_donasi
    if request.session.has_key('email'):
        cursor = connection.cursor()
        result = []
        try:
            email = request.session['email'][0]
            print(email)
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute(
                "delete from wishlist_donasi where email = '" + email + "' and idpd = '" + idPD + "';"
            )
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        return HttpResponseRedirect('/wishlist_donasi')
    else:
        return HttpResponseRedirect('/login/masuk')


def wishlist_add_view(request, id_donasi):
    idPD = id_donasi
    print(idPD)

    if request.session.has_key('email'):
        print("MASUK GAISKU")
        cursor = connection.cursor()
        result = []
        try:
            email = request.session['email'][0]
            print(email)
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute(
                "insert into wishlist_donasi values ('" + email + "', '" + idPD + "');"
            )
            result = namedtuplefetchall(cursor)
            print(result)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        return HttpResponseRedirect('/wishlist_donasi')
    else:
        print('GAMASUK GAIS')
        return HttpResponseRedirect('/login/masuk')