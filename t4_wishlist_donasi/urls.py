from django.urls import path

from . import views

urlpatterns = [
    path('', views.wishlist_view, name='index'),
    path('detail/<str:id_donasi>/', views.wishlist_detail_view, name='wishlist_detail'),
    path('delete/<str:id_donasi>/', views.wishlist_delete_view, name='wishlist_delete'),
    path('add/<str:id_donasi>/', views.wishlist_add_view, name='wishlist_add'),
]
app_name = 't4_wishlist_donasi'