from django.urls import path
from .views import *

app_name = 'myprofile'

urlpatterns = [
    path('', myprofile, name='myprofile'),
]