from django.shortcuts import render
from django.db import connection
from collections import namedtuple

# Function to return every row of data from query
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Create your views here.
def create(request):
    id = request.GET.get('id', '')

    return render(request, 'create.html', {'id' : id})