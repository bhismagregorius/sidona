from django.apps import AppConfig


class T3CatatanPengajuanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't3_catatan_pengajuan'
