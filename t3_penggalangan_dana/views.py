from django.shortcuts import render
from django.db import connection
from collections import namedtuple

# Function to return every row of data from query
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Create your views here.
def index(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO sidona")
        comm = "SELECT P.id, judul, kota, provinsi, tanggal_aktif_akhir, get_sisa_hari(P.id), jumlah_dibutuhkan, nama_kategori"
        comm += " FROM PENGGALANGAN_DANA_PD as P, KATEGORI_PD as K"
        comm += " WHERE K.id = P.id_kategori AND status_verifikasi = 'Terverifikasi'"
        cursor.execute(comm)
        result = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'index.html', {'result': result})

def idxAdmin(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO sidona")
        comm = "SELECT P.id, judul, kota, provinsi, tanggal_aktif_awal, tanggal_aktif_akhir, get_sisa_hari(P.id), jumlah_dibutuhkan, nama_kategori, status_verifikasi"
        comm += " FROM PENGGALANGAN_DANA_PD as P, KATEGORI_PD as K"
        comm += " WHERE K.id = P.id_kategori"
        cursor.execute(comm)
        result = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'idxAdmin.html', {'result': result})

def update(request):
    return render(request, 'update.html')