from django.apps import AppConfig


class T3PenggalanganDanaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't3_penggalangan_dana'
