from django.urls import path
from .views import index, update, idxAdmin

app_name = 't3_penggalangan_dana'
urlpatterns = [
    path('', index, name='index'),
    path('update', update, name='update'),
    path('admin', idxAdmin, name='idxAdmin')
]