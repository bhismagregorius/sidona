from django.shortcuts import render
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Create your views here.
def read_profile(request, email):
    cursor = connection.cursor()
    try:
        cursor.execute("SET SEARCH_PATH TO sidona")
        email_user = email # get email penggalang dana
        
        cursor.execute("SELECT * FROM penggalang_dana WHERE email = %s", [email_user])
        user = namedtuplefetchall(cursor)[0]
        cursor.execute("SELECT * FROM individu WHERE email = %s", [email_user])
        ind = namedtuplefetchall(cursor)
        cursor.execute("SELECT * FROM organisasi WHERE email = %s", [email_user])
        org = namedtuplefetchall(cursor)
        tipe = ""

        context = {'user': user}
        if (len(ind) != 0):
            ind = ind[0]
            tipe = "individu"
            context |= {'tipe': tipe, 'ind':ind}
        elif (len(org) != 0):
            org = org[0]
            tipe = "organisasi"
            context |= {'tipe': tipe, 'org':org}


        # print(ind)
        # print(org)
        cursor.close()
    except Exception as e:
        print(e)
    return render(request, 'read_profile.html', context)