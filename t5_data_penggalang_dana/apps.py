from django.apps import AppConfig


class T5DataPenggalangDanaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't5_data_penggalang_dana'
