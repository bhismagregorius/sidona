from django.urls import path
from .views import *

urlpatterns = [
    path('<str:email>', read_profile, name='read_profile'),
]
