from django.apps import AppConfig


class T1RegisterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't1_register'
