from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('admin/', views.register_admin, name='register_admin'),
    path('individu/', views.register_individu, name='register_individu'),
    path('organisasi/', views.register_organisasi, name='register_organisasi'),
]
app_name = 't1_register'
