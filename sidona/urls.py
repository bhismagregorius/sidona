"""sidona URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('main.urls', 'main'), namespace='main')),
    path('register/', include(('t1_register.urls', 't1_register'), namespace='t1_register')),
    path('login/', include(('t1_login.urls', 't1_login'), namespace='t1_login')),
    path('myprofile/', include(('myprofile.urls', 'myprofile'), namespace='myprofile')),
    path('wishlist_donasi/', include(('t4_wishlist_donasi.urls', 't4_wishlist_donasi'), namespace='t4_wishlist_donasi')),
    path('donasi/', include(('t4_donasi.urls', 't4_donasi'), namespace='t4_donasi')),
    path('penggalangan_dana_3/', include(('t3_penggalangan_dana.urls', 't3_penggalangan_dana'), namespace='t3_penggalangan_dana')),
    path('catatan_pengajuan/', include('t3_catatan_pengajuan.urls')),
    path('kategori_pd/', include('t3_kategori_penggalangan_dana.urls')),
    path('data_penggalang_dana/', include('t5_data_penggalang_dana.urls')),
    path('riwayat_penggunaan_dana/', include('t5_riwayat_penggunaan_dana.urls')),
    path('penggalangan_dana/', include(('penggalangan_dana.urls', 'penggalangan_danas'), namespace='penggalangan_dana')),
    path('daftar_pengguna/', include('daftar_pengguna.urls')),
]
