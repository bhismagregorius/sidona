from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple

from penggalangan_dana.forms import RegisterFormPasien, RegisterFormRumahIbadah, RestisterFormPenggalanganDanaPd
import datetime

# Fungsi untuk fetch data dari database
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Main Page Penggalangan Dana
def penggalangan_dana(request):
    request.session['email'] = 'mcreus3@nba.com'
    request.session['role'] = 'individu'
    return render(request, 'penggalangan_dana.html')

# Melihat Penggalangan Dana Pribadi
def see_pd(request, id) :

    if id == "main":
        data_pd = []

        cursor = connection.cursor()
        cursor.execute("select * from sidona.penggalangan_dana_pd;")
        penggalangan_dana_pd = namedtuplefetchall(cursor)
        
        for data in penggalangan_dana_pd:
            if data.email_user == request.session['email']:
                dict = {}
                dict['id'] = data.id 
                dict['judul'] = data.judul
                dict['kota'] = data.kota
                dict['provinsi'] = data.provinsi
                dict['tgl_aktif_awal'] = data.tanggal_aktif_awal
                dict['deadline'] = data.tanggal_aktif_akhir
                dict['sisa_hari'] = data.sisa_hari
                dict['target_dana'] = data.jumlah_dibutuhkan
                dict['kategori'] = getnama_kategori(data.id_kategori)
                dict['status_verifikasi'] = data.status_verifikasi
                
                print(dict)
                data_pd.append(dict)

        return render(request, 'see_pd.html', {'data_pd' : data_pd})
    else :
        return see_pd_detail(request, id)

def see_pd_detail(request, id) :

    cursor = connection.cursor()
    cursor.execute("select * from sidona.penggalangan_dana_pd where id = %s;", [str(id)])
    penggalangan_dana = namedtuplefetchall(cursor)

    cursor = connection.cursor()
    cursor.execute("select * from sidona.pd_kesehatan, sidona.pasien where idpd = %s and idpasien = nik;", [str(id)])
    kesehatan = namedtuplefetchall(cursor)

    cursor = connection.cursor()
    cursor.execute("select * from sidona.pd_rumah_ibadah, sidona.KATEGORI_AKTIVITAS_PD_RUMAH_IBADAH where idpd = %s and idaktivitas = id;", [str(id)])
    ri = namedtuplefetchall(cursor)

    cursor = connection.cursor()
    cursor.execute("select * from sidona.catatan_pengajuan where idpd = %s;", [str(id)])
    catatan = namedtuplefetchall(cursor)

    cursor = connection.cursor()
    cursor.execute("select * from sidona.donasi where idpd = %s;", [str(id)])
    donatur = namedtuplefetchall(cursor)

    cursor = connection.cursor()
    cursor.execute("select * from sidona.riwayat_penggunaan_dana where idpd = %s;", [str(id)])
    riwayat = namedtuplefetchall(cursor)

    dict = {}
    dict['id'] = penggalangan_dana[0].id 
    dict['email'] = penggalangan_dana[0].email_user
    dict['judul'] = penggalangan_dana[0].judul
    dict['deskripsi'] = penggalangan_dana[0].deskripsi
    dict['kota'] = penggalangan_dana[0].kota
    dict['provinsi'] = penggalangan_dana[0].provinsi
    dict['deadline'] = str(penggalangan_dana[0].tanggal_aktif_akhir)
    dict['jumlah_target_dana'] = penggalangan_dana[0].jumlah_dibutuhkan
    dict['kategori'] = getnama_kategori(penggalangan_dana[0].id_kategori)
    dict['link_repo'] = penggalangan_dana[0].link_repo
    dict['jumlah_terkumpul'] = penggalangan_dana[0].jumlah_terkumpul
    dict['jumlah_terpakai'] = penggalangan_dana[0].jumlah_terpakai
    
    dict['tgl_aktif_awal'] = penggalangan_dana[0].tanggal_aktif_awal
    dict['sisa_hari'] = penggalangan_dana[0].sisa_hari
    dict['status_verifikasi'] = penggalangan_dana[0].status_verifikasi

    if (dict['kategori'] == "kesehatan" and len(kesehatan) > 0) :
        dict['nik'] = kesehatan.idpasien
        dict['name'] = kesehatan.nama
        dict['penyakit'] = kesehatan.penyakit
        dict['komorbid'] = [] 

        cursor = connection.cursor()
        cursor.execute("select * from sidona.komorbid where idpd = %s;", [str(id)])
        komorbid = namedtuplefetchall(cursor)

        for i in komorbid:
            dict2 = {}
            dict2['name'] = i.komorbid;
            
            dict['komorbid'].append(dict2)

    elif (dict['kategori'] == 'rumah ibadah' and len(ri) > 0) :
        print(ri)
        dict['nomorsertifikat'] = ri.idrumahibadah
        dict['aktivitas'] = ri.nama


    dict['catatan'] = []
    
    for note in catatan:
        dict2 = {}
        dict2['timestamp'] = note.timestamp;
        dict2['informasi'] = note.informasi;

        dict['catatan'].append(dict2)

    dict['donatur'] = []
    
    for donate in donatur:
        dict2 = {}
        dict2['email'] = donate.email;
        dict2['timestamp'] = donate.timestamp;
        dict2['nominal'] = donate.nominal;
        dict2['doa'] = donate.doa;

        dict['donatur'].append(dict2)

    dict['riwayat'] = []

    for i in riwayat:
        dict2 = {}
        dict2['timestamp'] = i.timestamp;
        dict2['nominal'] = i.nominal;
        dict2['deskripsi'] = i.deskripsi;

        dict['riwayat'].append(dict2)

    return render(request, 'see_pd_detail.html', {'data' : dict})


# Melihat Penggalangan Dana Pribadi
def see_all_pd(request):
    data_pd = []

    cursor = connection.cursor()
    cursor.execute("select * from sidona.penggalangan_dana_pd;")
    penggalangan_dana_pd = namedtuplefetchall(cursor)
    
    for data in penggalangan_dana_pd:
        if data.status_verifikasi == "Terverifikasi" :
            dict = {}
            dict['id'] = data.id 
            dict['judul'] = data.judul
            dict['kota'] = data.kota
            dict['provinsi'] = data.provinsi
            dict['tgl_aktif_awal'] = data.tanggal_aktif_awal
            dict['deadline'] = data.tanggal_aktif_akhir
            dict['sisa_hari'] = data.sisa_hari
            dict['target_dana'] = data.jumlah_dibutuhkan
            dict['kategori'] = getnama_kategori(data.id_kategori)
            dict['status_verifikasi'] = data.status_verifikasi

            data_pd.append(dict)

    return render(request, 'see_all_pd.html', {'data_pd' : data_pd})

# Membuat Penggalangan Dana
def create_pd(request):

    if request.method == 'POST':
        kategori = request.POST.get('kategori')
        request.session['kategori'] = kategori # SESSION KATEGORI

        if kategori == 'kesehatan' or kategori == 'rumah ibadah':
            return HttpResponseRedirect('/penggalangan_dana/create_pd/opsi')
        elif kategori == 'bencana' or kategori == 'pendidikan' or kategori == 'sosial' :
            return HttpResponseRedirect('/penggalangan_dana/create_pd/form')

    return render(request, 'create_pd.html')

# Form untuk menanyakan iya/tidak
def before_initial_form(request) :

    if request.method == 'POST':

        opsi = request.POST.get("opsi")

        if opsi == 'Ya' :
            return HttpResponseRedirect('/penggalangan_dana/create_pd/in_form')
        elif opsi == 'Tidak' :
            return HttpResponseRedirect('/penggalangan_dana/create_pd/check_data')

    return render(request, 'form_before_initial.html')


# Form khusus rumah ibadah dan kesehatan (X)
def initial_form(request) :

    if(request.method == 'POST'):
        print("POST")
        print(request.session['kategori'])

        if request.session['kategori'] == 'kesehatan' :
            form = RegisterFormPasien(request.POST or None)

            if form.is_valid() :
                nik = request.POST.get("nik")
                nama = request.POST.get("nama")
                ttl = request.POST.get("ttl")
                alamat = request.POST.get("alamat")
                pekerjaan = request.POST.get("pekerjaan")

                # INSERT PASIEN
                insert_pasien(nik, nama, ttl, alamat, pekerjaan)

                request.session['nik'] = nik # SESSION NIK
                request.session['nama'] = nama # SESSION NAMA
                request.session['ttl'] = ttl
                request.session['alamat'] = alamat
                request.session['pekerjaan'] = pekerjaan
                request.session['id'] = generate_idpd()

                return HttpResponseRedirect('/penggalangan_dana/create_pd/form')

        elif request.session['kategori'] == "rumah ibadah" :
            print("rumah_ibadah")
            form = RegisterFormRumahIbadah(request.POST or None)

            if form.is_valid() :
                print("valid")
                nosert = request.POST.get("nomorsertifikat")
                nama = request.POST.get("nama")
                alamat = request.POST.get("alamat")
                jenis = request.POST.get("jenis")

                # INSERT RUMAH_IBADAH
                insert_rumah_ibadah(nosert, nama, alamat, jenis)

                request.session['nomorsertifikat'] = nosert # SESSION NOSERT
                request.session['nama'] = nama # SESSION NOSERT
                request.session['alamat'] = alamat # SESSION NOSERT
                request.session['jenis'] = jenis # SESSION NOSERT
                request.session['id'] = generate_idpd()

                return HttpResponseRedirect('/penggalangan_dana/create_pd/form')

    return render(request, 'form_initial.html')

def insert_pasien(nik, nama, ttl, alamat,pekerjaan):
    cursor = connection.cursor()
    cursor.execute("insert into sidona.pasien (nik,nama,tanggallahir, alamat, pekerjaan) values(%s,%s,%s,%s, %s)",[nik,nama,ttl, alamat, pekerjaan])

def insert_rumah_ibadah(nosert, nama, alamat, jenis):
    cursor = connection.cursor()
    cursor.execute("insert into sidona.rumah_ibadah (nosertifikat , nama, alamat, jenis) values(%s,%s,%s, %s)",[nosert, nama, alamat, jenis])

def generate_idpd() :

    cursor = connection.cursor()
    cursor.execute("select max(id) from sidona.penggalangan_dana_pd")
    ans = namedtuplefetchall(cursor)[0].max

    print(ans)

    ans = int(ans) + 1

    return ans

# Check apakah rumah ibadah ataupun kesehatan ada (X)
def check_data(request) :

    if request.method == 'POST':
 
        if request.session['kategori'] == 'kesehatan' :
 
            if is_exist_nik(request.POST.get("nik"), request) :
                request.session['truth'] = 'true'
            else :
                request.session['truth'] = 'false'
            
        elif request.session['kategori'] == "rumah ibadah" :

            if is_exist_nosert(request.POST.get("nomorsertifikat"), request) :                
                request.session['truth'] = 'true'
            else :
                request.session['truth'] = 'false'

        return HttpResponseRedirect('/penggalangan_dana/create_pd/check_data_2/')
    
    return render(request, 'check_data.html')

def is_exist_nik(nik, request) :
    cursor = connection.cursor()
    cursor.execute("select * from sidona.pasien;")
    all_registered_nik = namedtuplefetchall(cursor)
    result = False
    for data in all_registered_nik:
        if data.nik == nik:
            result = True
            request.session['nik'] = nik
            request.session['nama'] = data.nama
            request.session['tgl'] = str(data.tanggallahir)
            request.session['alamat'] = data.alamat
            request.session['pekerjaan'] = data.pekerjaan
            
            break
    return result

def is_exist_nosert(nomorsertifikat, request) :
    cursor = connection.cursor()
    cursor.execute("select * from sidona.rumah_ibadah;")
    all_registered_sert = namedtuplefetchall(cursor)
    result = False
    for data in all_registered_sert :
        if data.nosertifikat == nomorsertifikat:
            result = True
            request.session['nomorsertifikat'] = nomorsertifikat
            request.session['nama'] = data.nama
            request.session['alamat'] = data.alamat
            request.session['jenis'] = data.jenis

            break
    return result


def check_data_2(request) :

    return render(request, 'check_data_2.html')

# Form 
def ended_form(request) :

    form = RestisterFormPenggalanganDanaPd()

    if request.method == 'POST'  :

        print(request.POST)
        print(request.session['kategori'])
        id = request.session['id']
        judul = request.POST.get("judul")
        deskripsi = request.POST.get("deskripsi")
        kota = request.POST.get("kota")
        provinsi = request.POST.get("provinsi")
        link_repo = request.POST.get("berkas")
        status_verifikasi = "Belum Terverifikasi"
        tanggalsubmit = str(datetime.date.today())
        jumlah_dibutuhkan = request.POST.get("jumlah")
        email_user = request.session['email']
        id_kategori = getid_kategori(request.session['kategori'])

        if is_exits_user(email_user) :
            insert_penggalangan_dana_pd(id, judul, deskripsi, kota, provinsi, link_repo, status_verifikasi, tanggalsubmit, jumlah_dibutuhkan, email_user, id_kategori)

            return HttpResponseRedirect('/penggalangan_dana/see_pd/')

    return render(request, 'form_ended.html')

def is_exits_user(email_user) :
    cursor = connection.cursor()
    cursor.execute("select * from sidona.individu;")
    all_registered_sert = namedtuplefetchall(cursor)
    result = False
    for data in all_registered_sert :
        if data.email == email_user:
            result = True

            break
    return result

def getid_kategori (nama_kategori) :
    cursor = connection.cursor()
    cursor.execute("select * from sidona.kategori_pd;")
    all_registered_sert = namedtuplefetchall(cursor)
    result = False
    for data in all_registered_sert :
        if data.nama_kategori == nama_kategori:
            return data.id
    return 

def getnama_kategori (id_kategori) :
    cursor = connection.cursor()
    cursor.execute("select * from sidona.kategori_pd;")
    all_registered_sert = namedtuplefetchall(cursor)
    result = False
    for data in all_registered_sert :
        if data.id == id_kategori:
            return data.nama_kategori
    return 

def insert_penggalangan_dana_pd(id, judul, deskripsi, kota, provinsi, link_repo, status_verifikasi, tanggalsubmit, jumlah_dibutuhkan, email_user, id_kategori):
    cursor = connection.cursor()
    cursor.execute("insert into sidona.penggalangan_dana_pd (id, judul, deskripsi, kota, provinsi, link_repo, status_verifikasi, tanggalsubmit, jumlah_dibutuhkan, email_user, id_kategori) values(%s,%s,%s, %s, %s,%s,%s, %s, %s,%s,%s)",[id, judul, deskripsi, kota, provinsi, link_repo, status_verifikasi, tanggalsubmit, jumlah_dibutuhkan, email_user, id_kategori])

def kelola_penyakit(request) :

    data = []

    cursor = connection.cursor()
    cursor.execute("select * from sidona.penggalangan_dana_pd as p, sidona.pd_kesehatan as k where p.id = k.idpd and p.email_user = %s", [request.session['email']])
    kesehatan = namedtuplefetchall(cursor)

    for i in kesehatan :
        data2 = {}

        data2['id'] = i.id
        data2['judul'] = i.judul
        data2['komorbid'] = []
        
        cursor = connection.cursor()
        cursor.execute("select komorbid from sidona.penggalangan_dana_pd as p, sidona.pd_kesehatan as k, sidona.komorbid as ko where ko.idpd = %s and p.id = k.idpd and p.email_user = %s", [i.id, request.session['email']])
        komorbid = namedtuplefetchall(cursor)

        data3 = []
        for j in komorbid :
            data3.append(j.komorbid)

        data2['komorbid'] = data3;
    
    data.append(data2)

    return render(request, 'kelola_penyakit.html', {"data" : data})

def add_penyakit(request) :

    if request.method == 'POST' :
        return HttpResponseRedirect('/penggalangan_dana/kelola_penyakit/')

    return render(request, 'form_addpenyakit.html')

def update_penyakit(request) :

    if request.method == 'POST' :
        return HttpResponseRedirect('/penggalangan_dana/kelola_penyakit/')

    return render(request, 'form_updatepenyakit.html')



