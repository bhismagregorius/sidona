from django import forms


class RegisterFormPasien(forms.Form):
    nik= forms.CharField(label='NIK',max_length=20)
    nama= forms.CharField(label='Nama',max_length=50)
    ttl=forms.DateField(label='TTL')
    alamat=forms.CharField(label='Alamat',max_length=50)
    pekerjaan=forms.CharField(label='Pekerjaan', max_length=50)

class RegisterFormRumahIbadah(forms.Form):
    nomorsertifikat = forms.CharField(label='Nomor Sertifikat',max_length=20)
    nama = forms.CharField(label='Nama',max_length=50)
    alamat = forms.CharField(label='Alamat',max_length=50)
    jenis = forms.CharField(label='Jenis',max_length=50)

class RestisterFormPenggalanganDanaPd(forms.Form):
    id = forms.CharField(max_length=20)
    judul =  forms.CharField(widget=forms.Textarea)
    deskripsi =  forms.CharField(widget=forms.Textarea)
    kota =  forms.CharField(max_length=50)
    provinsi =  forms.CharField(max_length=50)
    linkrepo =  forms.CharField(widget=forms.Textarea)
    statusverifikasi =  forms.CharField(max_length=20)
    tanggalsubmit =  forms.DateField()
    jumlahdibutuhkan = forms.FloatField()
    emailuser = forms.CharField(max_length=20)
    idkategori = forms.CharField(max_length=20)
