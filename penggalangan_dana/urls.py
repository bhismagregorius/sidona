from django.urls import path
from .views import *

app_name = 'penggalangan_dana'

urlpatterns = [
    path('', penggalangan_dana, name='penggalangan-dana'),
    path('create_pd/', create_pd, name='create-pd'),
    path('see_pd/<id>/', see_pd, name='see-pd'),
    path('see_pd_detail/<id>/', see_pd_detail, name='see-pd-id'),
    path('see_all_pd/', see_all_pd, name='see-all-pd'),
    path('create_pd/form/', ended_form, name='ended-form'),
    path('create_pd/opsi/', before_initial_form, name='before-initial-form'),
    path('create_pd/in_form/', initial_form, name='initial-form'),
    path('create_pd/check_data/', check_data, name='check-data'),
    path('create_pd/check_data_2/', check_data_2, name='check-data-2'),
    path('kelola_penyakit/', kelola_penyakit, name='kelola-penyakit'),
    path('add_penyakit/', add_penyakit, name='add-penyakit'),
    path('update_penyakit/', update_penyakit, name='update-penyakit'),
]
