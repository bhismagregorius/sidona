from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request):
    result = []
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        try:
            cursor = connection.cursor()
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute("SELECT * FROM penggalang_dana AS ADM WHERE ADM.email ='" + email + "' AND ADM.PASSWORD = '" + password + "'")
            result = cursor.fetchone()
            if result == None: 
                return HttpResponseNotFound("The user does not exist")

            cursor.execute("SET SEARCH_PATH TO public")
            request.session['email'] = [email, password, result]
        except Exception as e:
            print(e)
            cursor.close()
        finally:
            cursor.close()
        return HttpResponseRedirect('/login/masuk')

    else:
        return render(request, 'login.html', {})


def loggedInView(request):
   if request.session.has_key('email'):
        cursor = connection.cursor()
        result = []
        try:
            cursor.execute("SET SEARCH_PATH TO sidona")
            cursor.execute("SELECT * FROM penggalang_dana WHERE EMAIL = '"+ request.session['email'][0] +"'")
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'coba_login.html', {"result" : result})
   else:
        return HttpResponseRedirect('/login')


def logout(request):
   try:
        del request.session['email']
   except:
        pass
   return HttpResponseRedirect('/login')