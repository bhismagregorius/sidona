from django.apps import AppConfig


class T1LoginConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't1_login'
