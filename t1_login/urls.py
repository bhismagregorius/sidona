from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('masuk/', views.loggedInView, name='masuk'),
    path('logout/', views.logout, name='logout'),
]
app_name = 't1_login'